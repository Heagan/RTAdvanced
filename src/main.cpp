#include <ctime>
#include "rt.h"
#include "image.h"
#include "camera.h"
#include "shape.h"
#include "SDL.hpp"
#include "light.h"

t_Main	a;
OctTree	*boundingBox;

void	rayTrace() {
	std::cout	<< "TRACING\n";
	Camera		*camera = a.camera;
	ShapeSet	*scene	= a.scene;
	double		r, g, b;
	
	drawHitBounds(boundingBox);
	for (int x = 0; x < g_width; x++) {
		for (int y = 0; y < g_height; y++) {
			Vector2 screenCoord((2.0f*x) / g_width - 1.0f,
								(-2.0f*y) / g_height + 1.0f);
			Ray ray = camera->makeRay(screenCoord);
			Intersection intersection(ray);

			//if (calcInterShapes(boundingBox, intersection)) {
			if (a.scene->intersect(intersection)) {
				Intersection lightIntersection = intersection;

				if (boundingBox->intersectLights(lightIntersection)) {
					intersection.lightDistance = lightIntersection.lightDistance;
				}

				r = intersection.color.r * 255.0f * intersection.lightDistance;
				g = intersection.color.g * 255.0f * intersection.lightDistance;
				b = intersection.color.b * 255.0f * intersection.lightDistance;

				a.sdl->putpixel(x, y, Color(r, g, b));
			} else {
				a.sdl->putpixel(x, y, 0);
			}
		}
	}
	std::cout << "TRACED!\n";
}

bool	calcInterShapes(OctTree *o, Intersection &intersection) {
	bool f = false;
	bool r = false;

	if (o->doesIntersect(intersection.ray)) {
		if (o->octas.size() == 8) {
			for (int i = 0; i < 8; i++) {
				f = calcInterShapes(o->octas[i], intersection);
				r = r ? true : f;
			}
		} else if (o->intersectShapes(intersection)) {
			Intersection lightIntersection = intersection;

			lightIntersection.ray.origin = lightIntersection.ray.calculate(lightIntersection.t);
			if (boundingBox->intersectLights(lightIntersection)) {
				intersection.lightDistance = lightIntersection.lightDistance;
			}
			return true;
		}
	}
	return r;
}

bool	calcInterLights(OctTree *o, Intersection &intersection) {
	bool f = false;
	bool r = false;

	if (o->doesIntersect(intersection.ray)) {
		if (o->octas.size() == 8) {
			for (int i = 0; i < 8; i++) {
				f = calcInterLights(o->octas[i], intersection);
				r = r ? true : f;
			}
		} else if (o->intersectLights(intersection)) {
			return true;			
		}
	}
	return r;
}

void	drawRect(Vector min, Vector max, Color c, int i = 1) {
	if (i) {
		a.minimap->drawLine(0 + min.x, 250 + min.z, 0 + max.x, 250 + min.z, c);
		a.minimap->drawLine(0 + min.x, 250 + min.z, 0 + min.x, 250 + max.z, c);
		a.minimap->drawLine(0 + max.x, 250 + max.z, 0 + max.x, 250 + min.z, c);
		a.minimap->drawLine(0 + max.x, 250 + max.z, 0 + min.x, 250 + max.z, c);
	} else {
		a.minimap->drawLine(350 + min.x, 250 + min.y, 350 + max.x, 250 + min.y, c);
		a.minimap->drawLine(350 + min.x, 250 + min.y, 350 + min.x, 250 + max.y, c);
		a.minimap->drawLine(350 + max.x, 250 + max.y, 350 + max.x, 250 + min.y, c);
		a.minimap->drawLine(350 + max.x, 250 + max.y, 350 + min.x, 250 + max.y, c);
	}

}

void	drawHitBounds(OctTree *o) {
	Color c;

	drawRect(o->min, o->max, {255, 0, 0});
	if (o->octas.size() == 8)
		for (int i = 0; i < 8; i++) {
			drawRect(o->octas[i]->min, o->octas[i]->max, {255, 0, 0}, 1);
			drawHitBounds(o->octas[i]);
		}
}

void 	draw(OctTree *o) {
	if (o->octas.size() > 0)
	for (int i = 0; i < 8; i++) {
		drawRect(o->octas[i]->min, o->octas[i]->max, {255, 0, 0}, 1);
		drawRect(o->octas[i]->min, o->octas[i]->max, {255, 0, 0}, 0);
		draw(o->octas[i]);
	}
}

void	spawnSphere(Vector v, double size, Color c) {
	Sphere *sphere = new Sphere(v, size, c);
	a.scene->addShape(sphere);
}

void	spawnLight(Vector v, double size, E_ShapeType type) {
	Light *light = new Light(v, size, type);
	a.scene->addShape(light);
}

void	spawnPlane(Vector v, Vector n, Color c) {
	Plane *floor = new Plane(v, n, c);
	a.scene->addShape(floor);
}

void	init() {
	boundingBox = new OctTree(&a);

	a.sdl = new SDL(g_width, g_height);
	a.sdl->display();
	SDL_SetWindowPosition(a.sdl->window, 0, 200);

	a.minimap = new SDL(g_width, g_height);
	SDL_SetWindowPosition(a.minimap->window, g_width, 200);
	a.minimap->display();

	a.camera = new PerspectiveCamera(Point(-5.0f, 1.0f, 0.0f),
		Vector(0.0f, 1.0f, 0.0f), Vector(), 70.0f * PI / 180.0f,
		(double)g_width / (double)g_height);
	a.scene = new ShapeSet;
}

void	createScene() {
	spawnPlane({0.0f, 0.0f, 0.0f}, Vector(), {0.5f, 1.0f, 0.5f});
	
	for (int i = -20; i < 20; i += 2) {
		spawnSphere(
		   {5.0f * abs(i + i), 
			2.0f * abs(i + i), 
			5.0f * i + i},
			1, {
			1.0f * (abs(i) % 3 == 0), 
			1.0f * (abs(i) % 3 == 1), 
			1.0f * (abs(i) % 3 == 2)});
	}

	for ( double y = 0 ; y < 10 ; y++ ) {
		for ( double x = 0 ; x < 10 ; x++ ) {
		spawnSphere(
		{
			60,
			4 * y,
			4 * (x - 5)
			},
			2,
			{
			0,
			0,
			1
			});

		}
	}

	for (int i = 1; i < 50; i++) {
		spawnSphere(
		   {(100.0f * (rand() % i) - 50.0f), 
			1, 
			(100.0f * (rand() % i) - 50.0f)},
			1, {
			1.0f * (abs(i) % 3 == 0), 
			1.0f * (abs(i) % 3 == 1), 
			1.0f * (abs(i) % 3 == 2)});
	}
}

int		main(int argc, char *argv[]) {
	srand(1000);
	init();
	spawnPlane({0.0f, 0.0f, 0.0f}, Vector(), {0.5f, 1.0f, 0.5f});
	spawnLight({50, 25, 0}, 25, E_SPLight);
	spawnSphere({50, 0, 0}, 10, {1, 0, 0});
	//spawnLight({50, 10, 0}, 250, E_SPLight);
	//spawnSphere({50, 10, 20}, 5, {0, 0, 1});

	boundingBox->octavateWorld();

	time_t stime = time(NULL);

	rayTrace();

	a.sdl->display();
	a.minimap->display();
	std::cout << time(NULL) - stime << " seconds to trace" << std::endl;
	bool running = true;
	while (running) {
		a.sdl->handleInput();
		a.minimap->handleInput();
		SDL_Delay(16);
	}
}